<section class="footer">
  <div class="row">
    <div class="small-12 column">
      <img src="img/thelocker.png" class="footer-image" alt="The Locker" />
    </div>
  </div>

  <div class="row">
    <div class="small-12 column">
      <ul class="social">
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
      </ul>
    </div>
  </div>

  <div class="row">
    <div class="small-12 column">
      <ul class="links">
        <li><a href="#">Terms &amp; Conditions</a></li>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
    </div>
  </div>

  <div class="row">
    <div class="small-12 column">
      <div class="copy">
        <p>&copy; <?php echo date(Y); ?> - Point Locker, All Rights Reserved.</p>
      </div>
    </div>
  </div>
</section>
